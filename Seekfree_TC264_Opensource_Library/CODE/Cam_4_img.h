/*
 * img.h
 *
 *  Created on: 202247
 *      Author: LeeV
 */

#ifndef CODE_IMG_H_
#define CODE_IMG_H_


#include "common.h"

uint8 otsuThreshold(uint8 *image, uint16 col, uint16 row);
void binarization(uint8 image_threshold, uint8 *image, int w, int h);



#endif /* CODE_IMG_H_ */
