/*
 * Cam_4_img.c
 *
 *  Created on: 2022年4月16日
 *      Author: LeeV
 */

#include "Cam_4_img.h"
#define GrayScale 256

//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取灰度直方图并使用大津法求阈值
//  @param      *image          传入图像数组
//  @param      col             图像的宽
//  @param      row             图像的高
//  @return     threshold       二值化阈值
//  @note       IFX_ALIGN(4) uint8 mt9v03x_image[MT9V03X_H][MT9V03X_W];
//  Sample usage:               get_grayscale_histogram(mt9v03x_image[0], MT9V03X_W， MT9V03X_H)
//-------------------------------------------------------------------------------------------------------------------
uint8 otsuThreshold(uint8 *image, uint16 col, uint16 row)
{

    uint16 width = col;
    uint16 height = row;
    int pixelCount[GrayScale];
    float pixelPro[GrayScale];
    int i, j, pixelSum = width * height;
    uint8 threshold = 0;
    uint8* data = image;  //指向像素数据的指针
    for (i = 0; i < GrayScale; i++)
    {
        pixelCount[i] = 0;
        pixelPro[i] = 0;
    }

    //统计灰度级中每个像素在整幅图像中的个数
    for (i = 0; i < height; i++)
    {
        for (j = 0; j < width; j++)
        {
            pixelCount[(int)data[i * width + j]]++;  //将像素值作为计数数组的下标
        }
    }

    //计算每个像素在整幅图像中的比例
    float maxPro = 0.0;
    for (i = 0; i < GrayScale; i++)
    {
        pixelPro[i] = (float)pixelCount[i] / pixelSum;
        if (pixelPro[i] > maxPro)
        {
            maxPro = pixelPro[i];
        }
    }

    //遍历灰度级[0,255]
    float w0, w1, u0tmp, u1tmp, u0, u1, u, deltaTmp, deltaMax = 0;
    for (i = 0; i < GrayScale; i++)     // i作为阈值
    {
        w0 = w1 = u0tmp = u1tmp = u0 = u1 = u = deltaTmp = 0;
        for (j = 0; j < GrayScale; j++)
        {
            if (j <= i)   //背景部分
            {
                w0 += pixelPro[j];
                u0tmp += j * pixelPro[j];
            }
            else   //前景部分
            {
                w1 += pixelPro[j];
                u1tmp += j * pixelPro[j];
            }
        }
        u0 = u0tmp / w0;
        u1 = u1tmp / w1;
        u = u0tmp + u1tmp;
        deltaTmp = w0 * pow((u0 - u), 2) + w1 * pow((u1 - u), 2);
        if (deltaTmp > deltaMax)
        {
            deltaMax = deltaTmp;
            threshold = (uint8)i;
        }
    }

    return threshold;
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      图像水平膨胀
//  @param      *image          传入图像数组
//  @param      col             图像的宽
//  @param      row             图像的高
//  @note       IFX_ALIGN(4) uint8 mt9v03x_image[MT9V03X_H][MT9V03X_W];
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
//void img_erosion(uint8 *image, uint16 col, uint16 row)
//{
//    int i,j;
//
//    for(i = 1; i < col - 1; i ++)
//    {
//        for(j = 1; j < row - 1; j ++)
//        {
//            if(image[i][j] == 0 && (image[i - 1][j] == 1 || image[i + 1][j] == 1))
//            {
//                image[i][j] = 1;
//            }
//        }
//    }
//}

//
////十字路口布线算法
//void dealCrossroads()
//{
//    int L_Hh[MT9V03X_H] = {0};  //左右赛道宽度直方图数组
//    int R_Hh[MT9V03X_H] = {0};
//    int Hh;            //左右赛道直方图最远有效行
//    int Kk[20];
//
//    float L_k, R_k;
//
//    //定义十字路口相关坐标
//    int L_up_x, L_up_y, L_middle_x,  L_middle_y, L_down_x, L_down_y = 0;
//    int R_up_x, R_up_y, R_middle_x,  R_middle_y, R_down_x, R_down_y = 0;
//
//
//
//    //获取最大有效行
//    for(int i = MT9V03X_H - 2; i > 2; i--)
//    {
//        if(img[i][MT9V03X_W / 2 - 1] - img[i - 1][MT9V03X_W / 2 - 1] == 1)
//        {
//            Hh = i + 1;
//            break;
//        }
//    }
//
//    for(int i = MT9V03X_H - 2; i > Hh; i--)
//    {
//        L_Hh[i] = MT9V03X_W / 2;        //向左找跳变
//        for(int j = MT9V03X_W / 2; j > 1; j--)      //向左找下降沿
//        {
//            if(img[i][j] - img[i][j - 1] == 1)
//            {
//                L_Hh[i] = MT9V03X_W / 2 - j;
//                break;
//            }
//        }
//
//
//        R_Hh[i] = MT9V03X_W / 2 - 1;        //向右找跳变
//        for(int j = MT9V03X_W / 2 - 1; j < MT9V03X_W - 1; j++)      //向右找下降沿
//        {
//            if(img[i][j] - img[i][j + 1] == 1)
//            {
//                R_Hh[i] = j - 93;
//                break;
//            }
//        }
//    }
//
//
//    //左下和右下两点的横坐标
//    L_down_x = MT9V03X_W / 2 - L_Hh[MT9V03X_H - 2];
//    R_down_x = MT9V03X_W / 2 - 1 - R_Hh[MT9V03X_H - 2];
//
//
//    L_middle_x = MT9V03X_W / 2 - L_Hh[Hh  + 1];
//    L_middle_y = Hh + 1;
//
//   // R_middle_x =
//
//}


