/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		headfile
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/
 
#ifndef _headfile_h
#define _headfile_h




#include "SEEKFREE_PRINTF.h"

#include "zf_assert.h"
#include "stdio.h"
#include "math.h"
//官方头文件
#include "ifxAsclin_reg.h"
#include "SysSe/Bsp/Bsp.h"
#include "IfxCcu6_Timer.h"
#include "IfxScuEru.h"

//------逐飞科技单片机外设驱动头文件
#include "zf_gpio.h"
#include "zf_gtm_pwm.h"
#include "zf_uart.h"
#include "zf_ccu6_pit.h"
#include "zf_stm_systick.h"
#include "zf_spi.h"
#include "zf_eru.h"
#include "zf_eru_dma.h"
#include "zf_vadc.h"
#include "zf_gpt12.h"
#include "zf_eeprom.h"

//------逐飞科技产品驱动头文件
#include "SEEKFREE_18TFT.h"
#include "SEEKFREE_FONT.h"
#include "SEEKFREE_FUN.h"
#include "SEEKFREE_IIC.h"
#include "SEEKFREE_IPS114_SPI.h"
#include "SEEKFREE_OLED.h"
#include "SEEKFREE_VIRSCO.h"
#include "SEEKFREE_MT9V03X.h"
#include "SEEKFREE_ICM20602.h"
#include "SEEKFREE_MPU6050.h"
#include "SEEKFREE_MMA8451.h"
#include "SEEKFREE_L3G4200D.h"
#include "SEEKFREE_WIRELESS.h"
#include "SEEKFREE_IPS200_PARALLEL8.h"
#include "SEEKFREE_7725.h"
#include "SEEKFREE_RDA5807.h"
#include "SEEKFREE_7725_UART.h"
#include "SEEKFREE_BLUETOOTH_CH9141.h"
#include "SEEKFREE_GPS_TAU1201.h"

#include "isr.h"

#include "Cam_4_img.h"
#endif
extern uint8 DIS_MODE;

extern uint8 image_threshold;
extern uint8 see_img[48][94];
extern int8 point[20][2];

extern uint16 setspeed;
extern int16 Set_speed;


extern int8 SZLK_STEP;
extern uint16 SZLK_L;

extern int8 SC_STEP;
extern int8 SC_DIR;
extern int8 SC_N;
extern float SC_ANG;
extern uint16 SC_L;

extern int8 YH_STEP;
extern int8 YH_DIR;
extern int8 YH_N;
extern float YH_ANG;
extern uint16 YH_L;

extern int8 BMX_STEP;
extern int8 BMX_DIR;
extern uint8 BMX_T;
extern uint8 BMX_N;
extern uint16 BMX_L;

extern int8 P_STEP;
extern int8 P_DIR;
extern int8 P_N;
extern uint16 P_L;
extern float P_ANG;

extern uint16 POWER_V;
extern int16 Car_speed;
extern int16 SPEED_init;

extern uint8 GB_H;
extern uint8 GB_MODE;
extern int16 Y_KP;//35
extern int16 DIR_KP;//120
extern int16 DIR_KD; //-140
extern int8 ForesightMax;

extern uint16 FC_L;   //发车路程积分
extern float FC_ANG; //发车角度积分
extern int8 GO_DIR;
extern int8 CAR_MODE;

extern int16 Z_gyro;
extern int16 Y_gyro;

extern int8 PO_STEP;
extern uint16 PO_L;

extern int8 TJP;

extern int8 HD_ERR;

extern uint8 TU_N;
extern uint8 TU_F;

extern int16 MAX_Y_GYRO;
extern int16 MIN_Y_GYRO;
