/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/


#include "headfile.h"
#pragma section all "cpu0_dsram"
//将本语句与#pragma section all restore语句之间的全局变量都放在CPU0的RAM中

#define X 0
#define Y 1

uint8 SW_KEY=1;
uint8 SW_KEY_OLD=1;

uint8 ROOL_DIR=1;

uint8 SW_ROOL=1;
uint8 SW_ROOL_OLD=1;

uint16 TT_n=0;
uint8 DIS_MODE=0;

//工程导入到软件之后，应该选中工程然后点击refresh刷新一下之后再编译
//工程默认设置为关闭优化，可以自己右击工程选择properties->C/C++ Build->Setting
//然后在右侧的窗口中找到C/C++ Compiler->Optimization->Optimization level处设置优化等级
//一般默认新建立的工程都会默认开2级优化，因此大家也可以设置为2级优化

//对于TC系列默认是不支持中断嵌套的，希望支持中断嵌套需要在中断内使用enableInterrupts();来开启中断嵌套
//简单点说实际上进入中断后TC系列的硬件自动调用了disableInterrupts();来拒绝响应任何的中断，因此需要我们自己手动调用enableInterrupts();来开启中断的响应。
int core0_main(void)
{
	get_clk();//获取时钟频率  务必保留
	//用户在此处调用各种初始化函数等

    gpt12_init(GPT12_T2, GPT12_T2INB_P33_7, GPT12_T2EUDB_P33_6);
    gpt12_init(GPT12_T4, GPT12_T4INA_P02_8, GPT12_T4EUDA_P00_9);

    gtm_pwm_init(ATOM0_CH0_P21_2, 7500, 0);
    gtm_pwm_init(ATOM0_CH1_P21_3, 7500, 0);
    gtm_pwm_init(ATOM0_CH2_P21_4, 7500, 0);
    gtm_pwm_init(ATOM0_CH3_P21_5, 7500, 0);

    gpio_init(P02_5, GPI, 1, NO_PULL);
    gpio_init(P02_6, GPI, 1, NO_PULL);
    gpio_init(P20_3, GPI, 1, NO_PULL);

    gpio_init(P22_0, GPI, 1, NO_PULL);
    gpio_init(P22_1, GPI, 1, NO_PULL);
    gpio_init(P22_2, GPI, 1, NO_PULL);
    gpio_init(P22_3, GPI, 1, NO_PULL);


    pit_interrupt_ms(CCU6_0, PIT_CH0, 10);
  //  pit_interrupt_ms(CCU6_0, PIT_CH1, 10);

    //等待所有核心初始化完毕
	IfxCpu_emitEvent(&g_cpuSyncEvent);
	IfxCpu_waitEvent(&g_cpuSyncEvent, 0xFFFF);
	enableInterrupts();

	while (TRUE)
	{
	    TT_n++;



	   // if(TT_n%16==0 && DIS_MODE )   UART_GO();

	    if(TT_n%32==0 )
	    {


	        POWER_V = (int)(adc_convert(ADC_0, ADC0_CH0_A0, ADC_12BIT)/4.05);

	        if(!DIS_MODE)      //显示设置
            {
	            dis_oled();
            }
	        else
	        {
	           oled_dis_bmp(48 , 94, see_img[0], image_threshold); //显示图像

	           dis_num(0,6,point[5][X],point[5][Y]); //查看关键点
	           dis_num(32,6,point[6][X],point[6][Y]);
	           dis_num(65,6,point[8][X],point[8][Y]);
	           dis_num(97,6,point[9][X],point[9][Y]);

	           dis_num(0,7,YH_STEP,P_STEP);
	           dis_num(32,7,point[1][X],point[1][Y]);
	           dis_num(65,7,point[3][X],point[3][Y]);
	           dis_num(97,7,BMX_T,SC_STEP);

	           dis_num(94,0,point[14][X],point[14][Y]);
	           dis_num(94,1,point[11][X],point[11][Y]);
	           dis_num(94,2,point[12][X],point[12][Y]);
	           dis_num(94,3,point[10][Y],point[13][Y]);
	           dis_num(94,4,point[7][X],point[7][Y]);
	           dis_num(94,5,point[2][X],point[2][Y]);
	        }
	    }

	   systick_delay_ms(STM0,5);


	   if(gpio_get(P22_0))  GO_DIR=1;//发车方向判别
       else GO_DIR=0;

       if(gpio_get(P22_1))  SC_DIR=1;//三岔方向判别
       else SC_DIR=0;

       if(gpio_get(P22_2))  DIS_MODE=1;//显示设置
       else DIS_MODE=0;

       if(gpio_get(P22_3))  TJP=1;//条件跑判别开启
       else TJP=0;

	   SW_KEY = gpio_get(P02_5);
       SW_ROOL = gpio_get(P20_3);
       //systick_delay_ms(STM0,5);
       ROOL_DIR = gpio_get(P02_6);

      // SW_KEY = 1;
      // SW_ROOL = 1;

       if(SW_KEY == 0 && SW_KEY_OLD == 1)  //编码器按下
       {
           gpio_set( P33_4, 0);
           GB_MODE = !GB_MODE;
           if(GB_MODE==1 && GB_H == 0)
           {
              switch(CAR_MODE)
              {
                  case 0: //正式发车,进入发车第一阶段
                  {
                      //oled_fill(0xff);
                      gpio_set( P33_4, 1);
                      systick_delay_ms(STM0,1000);
                      gpio_set( P33_4, 0);
                      PO_STEP=0;
                      CAR_MODE = 1;  //发车
                      FC_L = 0;      //发车路程控制
                      FC_ANG = 0;    //发车角度控制
                      BMX_STEP = 0;
                      YH_STEP = 0;
                      BMX_N = 2;
                      P_STEP = 0;
                      SC_STEP = 0;
                      PO_STEP = 0;
                      Set_speed = 80;  //一半速度启动
                  }break;
              }
           }
           else
           {
               CAR_MODE = 0;
               Set_speed = 0; //停车
           }

       }

       if(SW_ROOL == 0 && SW_ROOL_OLD == 1)  //编码器旋转
       {
           if(ROOL_DIR)  //正转
           {
               if(GB_MODE)
               {
                   switch(GB_H)
                   {
                       case 0:break;
                       case 1: HD_ERR +=1;break;
                       case 2:SPEED_init += 5;break;
                       case 3:DIR_KP += 1;break;
                       case 4:DIR_KD += 1;break;
                       case 5:Y_KP += 1;break;
                       case 6:ForesightMax += 1;break;
                       case 7:BMX_N += 1;break;
                   }
               }
               else
               {
                   if(GB_H<7)   GB_H +=1;
               }

           }
           else //翻转
           {
               if(GB_MODE)
               {
                   switch(GB_H)
                   {
                       case 0:break;
                       case 1:HD_ERR -=1;break;
                       case 2:SPEED_init -= 5;break;
                       case 3:DIR_KP -= 1;break;
                       case 4:DIR_KD -= 1;break;
                       case 5:Y_KP -= 1;break;
                       case 6:ForesightMax -= 1;break;
                       case 7:BMX_N -= 1;break;
                   }
               }
               else
               {
                   if(GB_H>0)   GB_H -=1;
               }

           }

       }

       SW_KEY_OLD = SW_KEY;
       SW_ROOL_OLD = SW_ROOL;
	}
}

#pragma section all restore


