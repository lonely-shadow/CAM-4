################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/IfxVadc_Adc.c 

OBJS += \
./Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/IfxVadc_Adc.o 

COMPILED_SRCS += \
./Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/IfxVadc_Adc.src 

C_DEPS += \
./Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/IfxVadc_Adc.d 


# Each subdirectory must supply rules for building sources it contributes
Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/%.src: ../Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/%.c
	@echo '正在构建文件： $<'
	@echo '正在调用： TASKING C/C++ Compiler'
	cctc -D__CPU__=tc26xb "-fC:/Users/32890/Desktop/CAM-4-master/Seekfree_TC264_Opensource_Library/Debug/TASKING_C_C___Compiler-Include_paths.opt" --iso=99 --c++14 --language=+volatile --exceptions --anachronisms --fp-model=3 -O0 --tradeoff=4 --compact-max-size=200 -g -Wc-w544 -Wc-w557 -Ctc26xb -o "$@"  "$<"  -cs --dep-file="$(@:.src=.d)" --misrac-version=2012 -N0 -Z0 -Y0 2>&1;
	@echo '已结束构建： $<'
	@echo ' '

Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/%.o: ./Libraries/infineon_libraries/iLLD/TC26B/Tricore/Vadc/Adc/%.src
	@echo '正在构建文件： $<'
	@echo '正在调用： TASKING Assembler'
	astc -o  "$@" "$<" --list-format=L1 --optimize=gs
	@echo '已结束构建： $<'
	@echo ' '


