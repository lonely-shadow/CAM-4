/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/



#include "headfile.h"
#pragma section all "cpu1_dsram"
//将本语句与#pragma section all restore语句之间的全局变量都放在CPU1的RAM中


#define Y_KP 40.0 //21.0
#define DIR_KP 30.0 //60
#define DIR_KD -120.0 //-200
#define Q_KP 0.0
#define DJ_INIT 770
#define DJ_L 650
#define DJ_R 890


/**  @brief    使用起始行  */
#define ROAD_START_ROW     60

/**  @brief    使用结束行  */
#define ROAD_END_ROW       5

//void img_erosion(uint8* image, uint16 h, uint16 w);



uint8 image_threshold;  //图像阈值

uint8* p;
uint8 img[MT9V03X_H][MT9V03X_W];


//寻找中线
int midline[MT9V03X_H] = {0};
int nextLine[MT9V03X_H] = {0};
int liftLine[MT9V03X_H] = {0};
int rightLine[MT9V03X_H] = {0};
float ForesightMax = 12;
float err = 0.0;


int l = 0 ,r = MT9V03X_W - 1;


//void PID();
void findMidline();
void direction_PID();
//void uart_to_pc();
void dealWithRings();
void ringDetection();

void core1_main(void)
{
    uint8 i,j;

	disableInterrupts();
    IfxScuWdt_disableCpuWatchdog(IfxScuWdt_getCpuWatchdogPassword());
    //用户在此处调用各种初始化函数等
    //OLED初始化
    oled_init();
    oled_p6x8str(0, 0, "OLED      OK");


    //GPIO初始化
    gpio_init(P33_4, GPO, 0, PUSHPULL);
    gpio_init(P20_8, GPO, 0, PUSHPULL);
    gpio_init(P20_9, GPO, 0, PUSHPULL);
    oled_p6x8str(0, 1, "GPIO      OK");

    uart_init(UART_2, 9600, UART2_TX_P10_5, UART2_RX_P10_6);

    //模拟IIC初始化
    simiic_init();
    oled_p6x8str(0, 2, "IIC       OK");

    //mpu6050初始化
    mpu6050_init();
    oled_p6x8str(0, 3, "MPU6050   OK");

    gtm_pwm_init(ATOM2_CH1_P33_5, 50, DJ_INIT);

    //摄像头初始化
    mt9v03x_init();
    oled_p6x8str(0, 4, "CAM       OK");


    oled_fill(0x00);

	//等待所有核心初始化完毕
	IfxCpu_emitEvent(&g_cpuSyncEvent);
	IfxCpu_waitEvent(&g_cpuSyncEvent, 0xFFFF);
    enableInterrupts();
    while (TRUE)
    {


		//用户在此处编写任务代码
        if(mt9v03x_finish_flag)
        {
            for(i = 0; i < MT9V03X_H; i++)
            {
                for(j = 0; j < MT9V03X_W; j++)
                {
                    img[i][j] = mt9v03x_image[i][j];
                }
            }
            p = img[0];
            //求阈值
            image_threshold = otsuThreshold(p, MT9V03X_H, MT9V03X_W);

            //二值化
            int i;
            for(i = 0; i < MT9V03X_H * MT9V03X_W; i++)
            {
                if(p[i] > image_threshold)
                {
                    p[i] = 0xFF;
                }
                else
                {
                    p[i] = 0x00;
                }
            }

            //seekfree_sendimg_03x(UART_2, img[0], 128, 64);

            findMidline();
            direction_PID();
            dealWithRings();
            //systick_delay_ms(STM0, 1000);
            //oled_dis_bmp(MT9V03X_H, MT9V03X_W, img[0], image_threshold);



            mt9v03x_finish_flag = 0;
        }
    }
}



//寻找中线
void findMidline()
{
    //中线数组赋初值
    for(int i = 0; i < MT9V03X_H; i++)
    {
        midline[i] = MT9V03X_W / 2;
        liftLine[i] = 0;
        rightLine[i] = MT9V03X_W - 1;
        nextLine[i] = MT9V03X_W / 2;
    }

    l=2;
    //求最近一行的中点
    for(int i = MT9V03X_W / 2; i > 2; i--)       //左
    {
        if(img[MT9V03X_H - 1][i] != img[MT9V03X_H - 1][i - 1])
        {
            l = i;
            break;
        }
    }
    r=MT9V03X_W - 2;
    for(int i = MT9V03X_W / 2; i < MT9V03X_W - 2; i++)      //右
    {
        if(img[MT9V03X_H - 1][i] != img[MT9V03X_H - 1][i + 1])
        {
            r = i;
            break;
        }
    }
    midline[MT9V03X_H - 1] = (l + r) / 2;       //求取并记录中点
    nextLine[MT9V03X_H - 2] = midline[MT9V03X_H - 1];       //迭代



    for(int i = MT9V03X_H - 2; i > 0; i--)
    {
        liftLine[i] = 0;
        rightLine[i] = MT9V03X_W - 1;
        //中线限幅
        if(nextLine[i] <= 2)
        {
            nextLine[i] = 2;
        }
        if(nextLine[i] >= 126)
        {
            nextLine[i] = 126;
        }


        for(int j = nextLine[i]; j > 1; j -= 1)
        {
            if(img[i][j] != img[i][j - 1])
            {
                liftLine[i] = j;
                break;
            }
        }

        for(int k = nextLine[i]; k < MT9V03X_W - 2; k += 1)
        {
            if(img[i][k] != img[i][k + 1])
            {
                rightLine[i] = k;
                break;
            }
        }
        nextLine[i - 1] = (liftLine[i] + rightLine[i]) / 2;
        midline[i] = (liftLine[i] + rightLine[i]) / 2;
     }
   // uint8 demo[MT9V03X_H][MT9V03X_W] = {0};
    for (int i = 0; i < MT9V03X_H - 1; i++)
    {

        img[i][midline[i]] = 0x00;
    }
    oled_dis_bmp(MT9V03X_H, MT9V03X_W, img, image_threshold);
}



//计算方向PID
void direction_PID()
{
    float curvature;    //曲率
    float A_x, A_y, B_x, B_y, C_x, C_y;     //定义A,B,C三个点的xy坐标
    float L_AB, L_BC, L_AC, K_AB, K_BC, COS_A, A;

    A_y = MT9V03X_H - 1;
    A_x = midline[MT9V03X_H - 1];
    A_y = MT9V03X_H - 1 - A_y;

    B_y = MT9V03X_H - 1 - ForesightMax;
    B_x = midline[(int)B_y];
    B_y = MT9V03X_H - 1- B_y;

    C_y = MT9V03X_H - 1 - 2 * ForesightMax;
    C_x = midline[(int)C_y];
    C_y = MT9V03X_H - 1- C_y;

    K_AB = (B_y - A_y) / (B_x - A_x);       //计算AB曲率
    K_BC = (C_y - B_y) / (C_x - B_x);       //计算BC曲率

    //AB 和 BC的曲率相等，三点共线，曲率 = 0
    if(K_AB == K_BC)
    {
        curvature = 0;
    }
    else
    {
        L_AB = sqrt((A_x - B_x) * (A_x - B_x) + (A_y - B_y) * (A_y - B_y));   // 求三边边长
        L_AC = sqrt((A_x - C_x) * (A_x - C_x) + (A_y - C_y) * (A_y - C_y));
        L_BC = sqrt((B_x - C_x) * (B_x - C_x) + (B_y - C_y) * (B_y - C_y));

        A = L_AC * L_AC + L_BC * L_BC - L_AB * L_AB;          //余弦定律
        COS_A = A / (2 * L_AC * L_BC);
        COS_A = sqrt(1 - COS_A * COS_A);      //求正弦
        curvature = COS_A / (0.5 * L_AB);


        if (C_x > B_x && B_x > A_x)            //判断曲率极性
        {
            curvature *= 100;
        }
        else if (C_x < B_x && B_x < A_x)
        {
            curvature *= -100;
        }
        else if (C_x > B_x && A_x > B_x)
        {
            curvature *= 100;
        }
        else {
            curvature *= -100;
        }
    }

    if((C_x < B_x && B_x < A_x) || (C_x > B_x && B_x > A_x))
    {
        err = 100 * (C_x - A_x) / (2 * ForesightMax);
    }
    else
    {
        err = 160 * (B_x - A_x) / ((1 * ForesightMax));
    }


    get_gyro();
    float gyro_z = mpu_gyro_z / 65.4;

    int dir_out = (int) (0.95 * (((A_x - 64) * Y_KP * -0.1) - curvature * Q_KP + gyro_z * DIR_KD * 0.01 - err * DIR_KP * 0.01));

    dir_out +=DJ_INIT;

    if(dir_out<DJ_L)dir_out=DJ_L;
    if(dir_out>DJ_R)dir_out=DJ_R;

    pwm_duty(ATOM2_CH1_P33_5,dir_out);



 //   gpio_set(P33_4, 1);
}


//环岛标志位
uint8 g_ucFlagRoundabout  = 0;

//圆环处理
void dealWithRings()
{
    if(g_ucFlagRoundabout == 0)
    {
        //圆环检测
        ringDetection();
    }
}


//圆环检测
void ringDetection()
{

    uint8 l_y = 0;
    uint8 l_x = 0;
    uint8 r_y = 0;
    uint8 r_x = 0;
    //已知最近行的左右两点是l, r
    uint8 L_OK=0;
    uint8 R_OK=0;

    for(int i = MT9V03X_H - 1; i > 1; i --)
    {
        for(int j = l - 2; j <  MT9V03X_W-1; j ++)
        {
            if(j == MT9V03X_W-2)
            {
                l_x = j;
                l_y = i;
                oled_int16(0, 0, l_x);
                oled_int16(0, 1, l_y);
                L_OK = 1;
            }
            if(img[i][j] == 0xff) //白点
            {
                i--;
                if(j>2) j-=2;
                else j=0;
                if(img[i][j] == 0xff)
                {
                    l_x = j;
                    l_y = i+1;
                    uart_send_int(UART_2, "l_x = ", l_x, "\n");
                    oled_int16(0, 0, l_x);
                    oled_int16(0, 1, l_y);
                    L_OK = 1;
                }
            }
            if(L_OK==1) break;
        }
        if(L_OK==1) break;
    }


       /* for(int j = r + 1; j > 64; j --)
        {
            if(img[i][j] == 0xff)
            {
                i --;
                if(img[i - 1][j] = 0xff)
                {
                    r_x = j;
                    r_y = i;
                    oled_int16(92, 0, r_x);
                    oled_int16(92, 1, r_y);
                    break;
                }
            }
        }*/


}



#pragma section all restore

