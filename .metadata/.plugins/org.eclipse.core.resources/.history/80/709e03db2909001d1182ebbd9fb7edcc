
 
/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		isr
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/


#include "isr_config.h"
#include "isr.h"
#include "stdlib.h"
#include "headfile.h"

#define VKp 62.5      //小车速度PID比例系数 62.5
#define VKi 3.72       //小车速度PID积分系数 3.72
#define VKd 0       //小车速度PID微分系数

int16 max = 9600;
#define MAX max    //限制最高车速

int16 speed1, speed2;       //用于存放编码器数据
int16 Car_speed;            //车身速度
int16 Set_speed = 0;        //设定速度
int16 Err_speed = 0;        //
int16 Err_speed_old = 0;    //
int16 PWM_speed = 0;        //

uint16 T0_N=0;

int16 Z_gyro=0;
int16 Z_gyro_old=0;
int16 Y_gyro;

//PIT中断函数  示例
IFX_INTERRUPT(cc60_pit_ch0_isr, 0, CCU6_0_CH0_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_0, PIT_CH0);

	if(T0_N++)
	if(T0_N%100==0)
	{
	    TU_F=TU_N;
	    TU_N=0;

	}
	//get_accdata();
	get_gyro();
	Z_gyro = (mpu_gyro_z / 65.4)*0.5+Z_gyro_old*0.5;
	Z_gyro_old = Z_gyro;
	Y_gyro = mpu_gyro_y;

	if(MAX_Y_GYRO<Y_gyro)    MAX_Y_GYRO=Y_gyro;
    if(MIN_Y_GYRO>Y_gyro)    MIN_Y_GYRO=Y_gyro;

    //获取当前车速
    speed1 = gpt12_get(GPT12_T2);
    speed2 = gpt12_get(GPT12_T4);

    Car_speed = speed2 - speed1;



    if(CAR_MODE)
    {
        if(FC_L < 60000)  FC_L += abs(Car_speed);
        FC_ANG += Z_gyro*0.04;
    }

    if(BMX_STEP)  if(BMX_L < 60000)  BMX_L += abs(Car_speed);

    if(PO_L < 60000)  PO_L += abs(Car_speed);

    if(P_STEP)
    {
        if(P_L < 60000)  P_L += abs(Car_speed);
        P_ANG += Z_gyro*0.04;
    }

    if(YH_L < 60000)  YH_L += abs(Car_speed);

    if(YH_STEP)
    {
        YH_ANG += Z_gyro*0.04;
    }

    if(SC_L < 60000)  SC_L += abs(Car_speed); //三岔路口积分路程控制
    SC_ANG += Z_gyro*0.04;

    if(SZLK_STEP) if(SZLK_L < 60000)  SZLK_L += abs(Car_speed); //十字路口积分路程控制

    gpt12_clear(GPT12_T2);
    gpt12_clear(GPT12_T4);

    //Set_speed = 100;
    //求偏差
    Err_speed = Set_speed - Car_speed;

    //PID计算
    PWM_speed += VKp * (Err_speed - Err_speed_old) + VKi * Err_speed;

    //迭代
    Err_speed_old = Err_speed;

    //限幅
    if(PWM_speed > 9600)
    {
        PWM_speed = 9600;
    }
    if(PWM_speed < -9600)
    {
        PWM_speed = -9600;
    }

   // PWM_speed = 4000;
//    //PWM响应
    if(PWM_speed>0)
    {
        pwm_duty(ATOM0_CH0_P21_2, 0);
        pwm_duty(ATOM0_CH1_P21_3, abs(PWM_speed));
        pwm_duty(ATOM0_CH2_P21_4, abs(PWM_speed));
        pwm_duty(ATOM0_CH3_P21_5, 0);
    }
    else
    {
        pwm_duty(ATOM0_CH0_P21_2, abs(PWM_speed));
        pwm_duty(ATOM0_CH1_P21_3, 0);
        pwm_duty(ATOM0_CH2_P21_4, 0);
        pwm_duty(ATOM0_CH3_P21_5, abs(PWM_speed));
    }
}


IFX_INTERRUPT(cc60_pit_ch1_isr, 0, CCU6_0_CH1_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_0, PIT_CH1);
//以下代码用于PWM调试
//    uart_putchar(UART_0, 0x03);
//    uart_putchar(UART_0, ~0x03);
//
//    uart_putchar(UART_0, (int)Set_speed);
//    uart_putchar(UART_0, (int)Set_speed >> 8);
//    uart_putchar(UART_0, (int)Err_speed);
//    uart_putchar(UART_0, (int)Err_speed>>8);
//    uart_putchar(UART_0, (int)Car_speed);
//    uart_putchar(UART_0, (int)Car_speed>>8);
//    uart_putchar(UART_0, (int)PWM_speed);
//    uart_putchar(UART_0, (int)PWM_speed>>8);
//
//    uart_putchar(UART_0, ~0x03);
//    uart_putchar(UART_0, 0x03);


}

IFX_INTERRUPT(cc61_pit_ch0_isr, 0, CCU6_1_CH0_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_1, PIT_CH0);

}

IFX_INTERRUPT(cc61_pit_ch1_isr, 0, CCU6_1_CH1_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_1, PIT_CH1);

}




IFX_INTERRUPT(eru_ch0_ch4_isr, 0, ERU_CH0_CH4_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH0_REQ4_P10_7))//通道0中断
	{
		CLEAR_GPIO_FLAG(ERU_CH0_REQ4_P10_7);
	}

	if(GET_GPIO_FLAG(ERU_CH4_REQ13_P15_5))//通道4中断
	{
		CLEAR_GPIO_FLAG(ERU_CH4_REQ13_P15_5);
	}
}

IFX_INTERRUPT(eru_ch1_ch5_isr, 0, ERU_CH1_CH5_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH1_REQ5_P10_8))//通道1中断
	{
		CLEAR_GPIO_FLAG(ERU_CH1_REQ5_P10_8);
	}

	if(GET_GPIO_FLAG(ERU_CH5_REQ1_P15_8))//通道5中断
	{
		CLEAR_GPIO_FLAG(ERU_CH5_REQ1_P15_8);
	}
}

//由于摄像头pclk引脚默认占用了 2通道，用于触发DMA，因此这里不再定义中断函数
//IFX_INTERRUPT(eru_ch2_ch6_isr, 0, ERU_CH2_CH6_INT_PRIO)
//{
//	enableInterrupts();//开启中断嵌套
//	if(GET_GPIO_FLAG(ERU_CH2_REQ7_P00_4))//通道2中断
//	{
//		CLEAR_GPIO_FLAG(ERU_CH2_REQ7_P00_4);
//
//	}
//	if(GET_GPIO_FLAG(ERU_CH6_REQ9_P20_0))//通道6中断
//	{
//		CLEAR_GPIO_FLAG(ERU_CH6_REQ9_P20_0);
//
//	}
//}



IFX_INTERRUPT(eru_ch3_ch7_isr, 0, ERU_CH3_CH7_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH3_REQ6_P02_0))//通道3中断
	{
		CLEAR_GPIO_FLAG(ERU_CH3_REQ6_P02_0);
		if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_vsync();
		else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_vsync();
		else if	(CAMERA_BIN       == camera_type)	ov7725_vsync();

	}
	if(GET_GPIO_FLAG(ERU_CH7_REQ16_P15_1))//通道7中断
	{
		CLEAR_GPIO_FLAG(ERU_CH7_REQ16_P15_1);

	}
}



IFX_INTERRUPT(dma_ch5_isr, 0, ERU_DMA_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套

	if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_dma();
	else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_dma();
	else if	(CAMERA_BIN       == camera_type)	ov7725_dma();
}


//串口中断函数  示例
IFX_INTERRUPT(uart0_tx_isr, 0, UART0_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart0_handle);
}
IFX_INTERRUPT(uart0_rx_isr, 0, UART0_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart0_handle);
}
IFX_INTERRUPT(uart0_er_isr, 0, UART0_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart0_handle);
}

//串口1默认连接到摄像头配置串口
IFX_INTERRUPT(uart1_tx_isr, 0, UART1_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart1_handle);
}
IFX_INTERRUPT(uart1_rx_isr, 0, UART1_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart1_handle);
    if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_uart_callback();
    else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_callback();
}
IFX_INTERRUPT(uart1_er_isr, 0, UART1_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart1_handle);
}


//串口2默认连接到无线转串口模块
IFX_INTERRUPT(uart2_tx_isr, 0, UART2_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart2_handle);
}
IFX_INTERRUPT(uart2_rx_isr, 0, UART2_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart2_handle);
    switch(wireless_type)
    {
    	case WIRELESS_SI24R1:
    	{
    		wireless_uart_callback();
    	}break;

    	case WIRELESS_CH9141:
		{
		    bluetooth_ch9141_uart_callback();
		}break;
    	default:break;
    }

}
IFX_INTERRUPT(uart2_er_isr, 0, UART2_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart2_handle);
}



IFX_INTERRUPT(uart3_tx_isr, 0, UART3_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart3_handle);
}
IFX_INTERRUPT(uart3_rx_isr, 0, UART3_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart3_handle);
    if(GPS_TAU1201 == gps_type)
    {
    	gps_uart_callback();
    }
}
IFX_INTERRUPT(uart3_er_isr, 0, UART3_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart3_handle);
}
